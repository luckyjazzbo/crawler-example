require "spec_helper"

describe Page do
  subject { Page.new("http://example.com/") }
  let(:linked_page) { Page.new("http://example.com/linked.html") }

  context "#score" do
    it "should be zero" do
      subject.num_inputs = 0
      expect(subject.score).to eq 0
    end

    it "should add number of inputs" do
      subject.num_inputs = 5
      expect(subject.score).to eq 5
    end

    it "should add number of inputs on linked pages" do
      subject.num_inputs = 0
      linked_page.num_inputs = 10
      subject.append_linked_page(linked_page)
      expect(subject.score).to eq 10
    end
  end

  context "#append_linked_page" do
    it "increases page depth" do
      subject.append_linked_page(linked_page)
      expect(linked_page.depth).to eq 1
    end

    it "does not overrides page depth" do
      linked_page.depth = 7
      subject.append_linked_page(linked_page)
      expect(linked_page.depth).to eq 7
    end
  end
end
