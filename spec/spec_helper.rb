$: << File.join(File.dirname(__FILE__), '..')

require "rspec/autorun"
require "webmock/rspec"

require "models/page"
require "services/indexer"
require "services/crawler"

RSpec.configure do |config|
  config.mock_with :mocha

  WebMock.disable_net_connect!(allow_localhost: true)

  config.order = "random"
end
