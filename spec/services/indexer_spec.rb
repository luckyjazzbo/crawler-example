require "spec_helper"

describe Indexer do
  context "#count_inputs" do
    it "returns 0 for empty string" do
      expect( subject.send(:count_inputs, "") ).to eq 0
    end

    it "returns correct number of inputs" do
      expect( subject.send(:count_inputs, "aaa <a href='http://example.com/'>home</a> bbb <input name='foo'/> ccc <input data-role='bar'>") ).to eq 2
    end
  end

  context "#each_linked_page" do
    it "yields nothing for empty page" do
      pages = []
      subject.send(:each_linked_page, "http://example.com/", "") do |page|
        pages << page
      end
      expect(pages).to be_empty
    end

    it "yields correct links" do
      html = %(
        <p>
          aaa
          <a href='/'>home</a>
          bbb
          <a href="http://test.com/">test</a>
          <A href=inner.html>inner</A>
        </p>
      )
      pages = []
      subject.send(:each_linked_page, "http://example.com/folder/start.html", html) do |page|
        pages << page
      end
      expect(pages.map(&:url)).to match_array(["http://example.com/", "http://test.com/", "http://example.com/folder/inner.html"])
    end
  end

  context "#no_jobs?" do
    it "there is no jobs by default" do
      expect(subject.send(:no_jobs?)).to be_true
    end
  end

  context "#page_limit_exceeded?" do
    it "returns false by default" do
      expect(subject.send(:page_limit_exceeded?)).to be_false
    end

    it "returns true" do
      Indexer::MAX_PAGES.times do |i|
        page = Page.new "http://example.com/page#{i}.html"
        page.html = ""
        subject.send(:index, page)
      end

      expect(subject.send(:page_limit_exceeded?)).to be_true
    end
  end

  context "#index_recursively" do
    it "indexes site" do
      stub_request(:get, "http://example.com/").to_return(body:
        %(<input>
          <a href="/foo.html">foo</a>
          <a href="/bar.html">bar</a>))
      stub_request(:get, "http://example.com/foo.html").to_return(body:
        %(<input><input>
          <a href="/bar.html">home</a>))
      stub_request(:get, "http://example.com/bar.html").to_return(body: "")
      subject.index_recursively("http://example.com/")
      expect(subject.send(:formatted_result)).to match_array([
        "http://example.com/ - 3 inputs",
        "http://example.com/foo.html - 2 inputs",
        "http://example.com/bar.html - 0 inputs"
      ])
    end
  end
end
