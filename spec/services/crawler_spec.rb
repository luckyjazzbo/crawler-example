require "spec_helper"

describe Crawler do
  let(:indexer) { mock("Indexer") }
  subject { Crawler.new(indexer) }

  context "#queue_page" do
    it "queue nils" do
      queue = mock("Queue")
      queue.expects(:<<).with(nil).once
      subject.instance_variable_set :@queue, queue
      subject.queue_page(nil)
    end

    it "does not queue duplicate pages" do
      queue = mock("Queue")
      queue.expects(:<<).once
      subject.instance_variable_set :@queue, queue
      subject.queue_page Page.new("http://example.com/")
      subject.queue_page Page.new("http://example.com/")
    end
  end

  context "#read_url_content" do
    it "returns content of target page" do
      stub_request(:get, "example.com").to_return(body: "abc")
      content = subject.send :read_url_content, "http://example.com/"
      expect(content).to eq "abc"
    end

    it "returns empty string on errors" do
      stub_request(:get, "example.com").to_return(status: [500, "Internal Server Error"])
      content = subject.send :read_url_content, "http://example.com/"
      expect(content).to eq ""
    end
  end

  context "#work" do
    it "exits when nil is queued" do
      subject.queue_page(nil)
      start_time = Time.now
      subject.send(:work)
      end_time = Time.now
      expect(end_time - start_time).to be < 1
    end

    it "should queue indexer with downloaded pages" do
      stub_request(:get, "example.com").to_return(body: "abc")
      indexer.expects(:queue_page).once
      subject.queue_page Page.new("http://example.com/")
      subject.queue_page(nil)
      subject.send(:work)
    end
  end
end
