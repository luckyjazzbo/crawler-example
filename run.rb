#!/usr/bin/env ruby

if ARGV.empty? || !(ARGV[0] =~ %r(https?://[\S]+))
  puts 'Usage: ./run.rb "http://example.com/start_page.html"'
else
  $: << File.dirname(__FILE__)

  require "models/page"
  require "services/crawler"
  require "services/indexer"

  indexer = Indexer.new
  indexer.index_recursively(ARGV[0])
  indexer.print_formatted_result
end
