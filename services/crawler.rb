require 'thread'
require 'set'
require 'open-uri'

class Crawler
  NUM_THREADS = 10

  attr_accessor :queue

  def initialize(indexer)
    @indexer = indexer
    @queue = Queue.new
    @lock = Mutex.new
    @queued_urls = Set.new
    @num_active_threads = 0
  end

  def queue_page(page)
    @lock.synchronize do
      if page
        unless @queued_urls.include? page.url
          @queue << page
          @queued_urls << page.url
        end
      else
        @queue << page
      end
    end
  end

  def run
    NUM_THREADS.times do
      Thread.new do
        work
      end
    end
  end

  def in_progress?
    @queue.size > 0 || @num_active_threads > 0
  end

  private

  def work
    loop do
      page = @queue.pop
      if page
        @lock.synchronize { @num_active_threads += 1 }
        page.html = read_url_content(page.url)
        @indexer.queue_page(page)
        @lock.synchronize { @num_active_threads -= 1 }
      else
        @queue << nil
        break
      end
    end
  end

  def read_url_content(url)
    open(url).read
  rescue => e
    ""
  end
end
