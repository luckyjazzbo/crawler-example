require 'thread'
require 'uri'

class Indexer
  MAX_PAGES = 50
  MAX_DEPTH = 3

  attr_accessor :queue, :crawler

  def initialize
    @crawler = Crawler.new(self)
    @queue = Queue.new
    @lock = Mutex.new
    @indexed_pages_hash = {}
  end

  def index_recursively(url)
    @crawler.queue_page Page.new(url)
    @crawler.run

    loop do
      index(@queue.pop)

      if finish_criteria_met?
        @crawler.queue_page nil
        break
      end
    end
  end

  def queue_page(page)
    @queue << page
  end

  def print_formatted_result
    puts "\n"
    formatted_result.each {|line| puts line}
  end

  private

  def index(page)
    page.num_inputs = count_inputs(page.html)

    each_linked_page(page.url, page.html) do |linked_page|
      page.append_linked_page(linked_page)

      if page.depth <= MAX_DEPTH + 1
        @crawler.queue_page(linked_page)
      end
    end

    @indexed_pages_hash[page.url] = page
  end

  def count_inputs(html)
    html.scan(/<input/i).size.to_i
  end

  def each_linked_page(base_url, html)
    urls = html.scan(/<a[^>]*href=['"]?([^'" >]+)/i).flatten
    prepare_urls(urls, base_url).each do |prepared_url|
      yield find_or_build_page(prepared_url)
    end
  end

  def find_or_build_page(url)
    @indexed_pages_hash[url] || (@indexed_pages_hash[url] = Page.new(url))
  end

  def prepare_urls(urls, base_url)
    parsed_base_url = URI.parse(base_url)
    filter_urls(urls).collect do |url|
      begin
        parsed_url = URI.parse(url)
        parsed_url.scheme ||= parsed_base_url.scheme
        parsed_url.host ||= parsed_base_url.host
        unless parsed_url.path.start_with? "/"
          folder_path = parsed_base_url.path[0..parsed_base_url.path.rindex("/")]
          parsed_url.path = "#{folder_path}#{parsed_url.path}"
        end
        parsed_url.to_s
      rescue => e
      end
    end.compact
  end

  def filter_urls(urls)
    urls.reject do |url|
      url.empty? ||
        url.start_with?("#") ||
        url.start_with?("\\") ||
        (url =~ /^[a-z]+:/i && !(url =~ /^http/i))
    end
  end

  def formatted_result
    @indexed_pages_hash.values.select(&:complete?).collect do |page|
      "#{page.url} - #{page.score} inputs"
    end
  end

  def finish_criteria_met?
    no_jobs? || page_limit_exceeded?
  end

  def no_jobs?
    @queue.size == 0 && !@crawler.in_progress?
  end

  def page_limit_exceeded?
    @indexed_pages_hash.values.select(&:complete?).size >= MAX_PAGES
  end
end
