class Page
  attr_accessor :url, :html, :depth, :num_inputs, :linked_pages

  def initialize(url)
    @url = url
    @depth = 0
    @linked_pages = []
  end

  def append_linked_page(page)
    page.depth = @depth + 1 if page.depth == 0
    @linked_pages << page
  end

  def score
    @score ||= num_inputs + num_inputs_on_linked_pages
  end

  def complete?
    @complete ||= @num_inputs != nil && @linked_pages.all?{|page| page.num_inputs != nil}
  end

  private

  def num_inputs_on_linked_pages
    @linked_pages.map(&:num_inputs).inject{|sum,x| sum + x}.to_i
  end
end
